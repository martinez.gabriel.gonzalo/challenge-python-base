from base64 import encode
from sqlalchemy import create_engine

from decouple import config

import pandas as pd

import logging

logging.basicConfig(filename=config('FILENAMELOG'), level=logging.DEBUG)

class BaseDeDatos:
    engine = create_engine("postgresql://{user}:{password}@{host}:{port}/{database}".format(
        user=config('USER'),
        password=config('PASSWORD'),
        host=config('HOST'),
        port=config('PORT'),
        database=config('DATABASE')
    ))
    
    def ejecutar_sql(self, sql):
        self.engine.execute(sql)

    def write(self, df:pd.DataFrame, name: str):
        try:
            #Guarda el contenido del dataframe en la base de datos
            df.to_sql(name=name, con=self.engine, if_exists='replace')
        except Exception as err:
            logging.error("No se pudieron guardar los datos correctamente en la base de datos: " + str(err))
