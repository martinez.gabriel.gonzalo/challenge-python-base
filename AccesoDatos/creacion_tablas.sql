DROP TABLE IF EXISTS DatosProcesados;
CREATE TABLE DatosProcesados (
    id INTEGER PRIMARY KEY,
    cod_localidad INTEGER NULL,
    id_provincia INTEGER NULL,
    id_departamento INTEGER NULL,
    categoria VARCHAR(20) NULL,
    provincia VARCHAR(100) NULL,
    localidad VARCHAR(100) NULL,
    nombre VARCHAR(100) NULL,
    domicilio VARCHAR(100) NULL,
    codigo_postal VARCHAR(20) NULL,
    numero_de_telefono VARCHAR(100) NULL,
    mail VARCHAR(200) NULL,
    web VARCHAR(200) NULL,
    fecha_carga DATE
);
DROP TABLE IF EXISTS CantidadRegistros;
CREATE TABLE CantidadRegistros(
    id INTEGER PRIMARY KEY,
    agrupador VARCHAR(100),
    cantidad_registros INTEGER
);
DROP TABLE IF EXISTS InformacionCines;
CREATE TABLE InformacionCines(
    id INTEGER PRIMARY KEY,
    provincia VARCHAR(100),
    cantidad_pantallas INTEGER,
    cantidad_butacas INTEGER,
    cantidad_espacios INTEGER
);

-- CREATE TABLE Test(cod_localidad INTEGER NULL);
-- SELECT * FROM test;
-- CREATE TABLE prueba(cod_localidad INTEGER NULL);