from decouple import config
import logging

from sqlalchemy import create_engine


engine = create_engine("postgresql://{user}:{password}@{host}:{port}/{database}".format(
    user=config('USER'),
    password=config('PASSWORD'),
    host=config('HOST'),
    port=config('PORT'),
    database=config('DATABASE')
))
with open("./creacion_tablas.sql", "r") as myfile:
    data = myfile.read()
    engine.execute(data)