import requests
import os

from .reader import Reader

class Archivo:

    def __init__(self, categoria, mes, url, anio, nombre_archivo):
        self.categoria = categoria
        self.mes = mes
        self.anio = anio
        self.url = url
        self.nombre_archivo = nombre_archivo
        self.url_carpeta = None
        self.r = None

    def creacion_carpeta(self) -> str:
        url_carpeta = "./{categoria}/{anio}-{mes}".format(
            categoria = self.categoria,
            anio = self.anio,
            mes = self.mes
        )
        if not os.path.isdir(url_carpeta):
            os.makedirs(url_carpeta)
        return url_carpeta

    def obtener_archivo(self) -> str:
        r = requests.get(self.url, allow_redirects=True)
        url_carpeta = self.creacion_carpeta()
        open(url_carpeta + "/" + self.nombre_archivo, "wb").write(r.content)
        self.url_carpeta = url_carpeta + "/" + self.nombre_archivo

    def dataframe_archivo_informacion(self, campos: list, campos_reemplazo: dict):
        self.r = Reader(self.url_carpeta, self.categoria.lower(), campos, campos_reemplazo)
        return self.r.procesar_archivo_informacion()

    def dataframe_registros(self, agrupador):
        return self.r.cantidad_registros(agrupador)

    def dataframe_agrupador_cine(self):
        return self.r.agrupador_provincia()

    