from unicodedata import name
import pandas as pd

from datetime import datetime

class Reader:
    
    def __init__(self, url_archivo: str, categoria: str, campos: list, campos_reemplazo: dict):
        self.url_archivo = url_archivo
        self.categoria = categoria
        self.campos = campos
        self.campos_reemplazo = campos_reemplazo
        self.df = pd.read_csv(self.url_archivo)

    def procesar_archivo_informacion(self) -> pd.DataFrame:
        df = self.df
        df = df.assign(categoria=self.categoria, fecha_carga=datetime.now().date())
        df = df[self.campos]
        df = df.rename(columns=self.campos_reemplazo)
        return df

    def cantidad_registros(self, agrupador: list) -> pd.DataFrame:
        df = self.df
        df = df.assign(categoria=self.categoria)
        if len(agrupador) == 1:
            df = df.groupby(agrupador)[agrupador[0]].count().reset_index(name="cantidad_registros")
            df = df.rename(columns={
                agrupador[0]:"agrupador"
            })
        else:
            df = df.assign(fecha_carga=datetime.now().date())
            df = df.groupby(agrupador)["fecha_carga"].count().reset_index(name="cantidad_registros")
            df["agrupador"] = df[agrupador[0]].map(str) + " - " + df[agrupador[1]].map(str)
            df = df[["agrupador", "cantidad_registros"]]
        return df

    def agrupador_provincia(self):
        df = self.df
        df["espacio_INCAA"] = df["espacio_INCAA"].replace({"SI":1, "Si":1, "si": 1, "":0})
        df["espacio_INCAA"] = df["espacio_INCAA"].fillna(0)
        df["espacio_INCAA"] = df["espacio_INCAA"].astype(int)
        df = df.groupby("Provincia")["Pantallas", "Butacas", "espacio_INCAA"].sum()
        df = df.rename(columns=
            {
                "Provincia":"provincia",
                "Pantallas":"cantidad_pantallas",
                "Butacas":"cantidad_butacas",
                "espacio_INCAA":"cantidad_espacios"
            }
        )
        return df

    