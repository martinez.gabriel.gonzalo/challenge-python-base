Pasos para levantar el proyecto:

1 - Crear un entorno virtual, con virtualenv. El nombre del entorno virtual puede ser cualquiera. En este caso, se llamará "venv"

```
virtualenv -p python3 venv
```

2 - Activar el entorno virtual

```
source venv/bin/activate
```

3 - Instalar las dependencias que vienen en el archivo "requeriments.txt"

```
pip install -r requeriments.txt
```

4 - Realizar las configuraciones necesarias para la conexión a la base de datos. Para esto, vamos al archivo .env del proyecto y cambiamos los valores de nuestra conexión

```
HOST="HOST"
DATABASE="DATABASE"
USER="USER"
PASSWORD="PASSWORD"
PORT="PORT"
FILENAMELOG="FILENAMELOG"
```

Aclaración: FILENAMELOG es para indicarle el nombre de archivo de LOG's que se van a ir generando en la ejecución del programa.

5 - Para crear las tablase en la base de datos, vamos a ir al script script_creacion_tablas.py que está dentro de la carpeta "AccesoDatos". Lo corremos con el siguiente comando (y el entorno virtual activado):

```
python3 script_creacion_tablas.py
```

6 - Ya podemos correr nuestro programa:

```
python3 main.py
```

7 - Para poder correr el bonus track, corremos el archivo "bonustrack.py":

```
python3 bonustrack.py
```

Aclaración: La versión del drive de chrome es la versión "ChromeDriver 104.0.5112.79". Verificar su navegador.
