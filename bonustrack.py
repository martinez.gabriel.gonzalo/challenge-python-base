from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By

import time

import pandas as pd

from openpyxl.utils.dataframe import dataframe_to_rows
from openpyxl import Workbook

#Opciones de navación
options = webdriver.ChromeOptions()
options.add_argument("--start-maximized")
options.add_argument("--disabled-extensions")

driver_path = "./chromedriver"
driver = webdriver.Chrome(driver_path, options=options)

#Inicializamos el navegador
driver.get("https://www.bna.com.ar/Personas")

#Espero un tiempo para que se cargue correctamente la página
time.sleep(6)


#Busco la fecha y la tabla para poder extraer los datos
fecha = driver.find_element(by=By.XPATH, value="/html/body/main/div/div/div[4]/div[1]/div/div/div[1]/table/thead/tr/th[1]")
tabla = driver.find_element(by=By.XPATH, value='/html/body/main/div/div/div[4]/div[1]/div/div/div[1]/table/tbody')
filas = tabla.find_elements(By.TAG_NAME, "tr")

#Creo el dataframe que después voy a utilizar para guardar los datos
df = pd.DataFrame(columns=["Moneda", "Compra", "Venta", "Promedio"])

#Recorro cada fila para sacar los datos y los voy agregando al dataframe
for fila in filas:
    celdas = fila.find_elements(By.TAG_NAME, "td")
    moneda = celdas[0].text
    compra = float(str(celdas[1].text).replace(",", "."))
    venta = float(str(celdas[2].text).replace(",", "."))
    promedio = (compra + venta) / 2
    lista_agregar = [moneda, compra, venta, promedio]
    df.loc[len(df)] = lista_agregar

#Agrego los datos y los exporto a un excel
wb = Workbook()
ws = wb.active
ws["A1"]="Día"
ws["B1"]=fecha.text
for r in dataframe_to_rows(df, index=False, header=True):
    ws.append(r)
wb.save("cotizacion.xlsx")