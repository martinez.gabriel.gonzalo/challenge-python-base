from base64 import encode
from datetime import datetime
from decouple import config

import pandas as pd

from sqlalchemy import create_engine

from AccesoDatos.conexion_bd import BaseDeDatos
from Datos.archivo import Archivo

from utils import MESES
from utils import CAMPOS_BIBLIOTECA
from utils import CAMPOS_MUSEO
from utils import CAMPOS_CINE
from utils import CAMPOS_REEMPLAZO_CINE
from utils import CAMPOS_REEMPLAZO_BIBLIOTECA
from utils import CAMPOS_REEMPLAZO_MUSEO

import logging

logging.basicConfig(filename=config('FILENAMELOG'), level=logging.DEBUG)

def descarga_archivos():
    """
    Descarga de archivos

    Esta función llama a la clase Archivo que es la engargada de obtener los archivos del cine, museo y biblioteca

    Retorna los archivos generados
    """

    try:
        fecha = datetime.now()
        cine = Archivo(
                "Cine",
                MESES.get(fecha.month).lower(),
                "https://datos.cultura.gob.ar/dataset/37305de4-3cce-4d4b-9d9a-fec3ca61d09f/resource/392ce1a8-ef11-4776-b280-6f1c7fae16ae/download/cine.csv",
                fecha.year,
                "{anio}-{mes}-{dia}-{nombre}".format(
                    anio=fecha.year,
                    mes=fecha.month,
                    dia=fecha.day,
                    nombre="cine.csv"
                )
            )
        museo = Archivo(
                "Museos",
                MESES.get(fecha.month).lower(),
                "https://datos.cultura.gob.ar/dataset/37305de4-3cce-4d4b-9d9a-fec3ca61d09f/resource/4207def0-2ff7-41d5-9095-d42ae8207a5d/download/museos_datosabiertos.csv",
                fecha.year,
                "{anio}-{mes}-{dia}-{nombre}".format(
                    anio=fecha.year,
                    mes=fecha.month,
                    dia=fecha.day,
                    nombre="museo.csv"
                )
            )
        biblioteca = Archivo(
                "Biblioteca",
                MESES.get(fecha.month).lower(),
                "https://datos.cultura.gob.ar/dataset/37305de4-3cce-4d4b-9d9a-fec3ca61d09f/resource/01c6c048-dbeb-44e0-8efa-6944f73715d7/download/biblioteca_popular.csv",
                fecha.year,
                "{anio}-{mes}-{dia}-{nombre}".format(
                    anio=fecha.year,
                    mes=fecha.month,
                    dia=fecha.day,
                    nombre="biblioteca.csv"
                )
            )
        cine.obtener_archivo()
        museo.obtener_archivo()
        biblioteca.obtener_archivo()
        logging.info("Se descargaron los archivos")
        return cine, museo, biblioteca
    except Exception as err:
        logging.error("Error inesperado en la descarga de archivos: " + str(err))

def procesar_informacion(cine: Archivo, museo: Archivo, biblioteca: Archivo) -> pd.DataFrame:
    """
    Procesar toda la información

    Esta función va a normalizar y unificar toda la información de Museos, Salas de Cine y Bibliotecas
    Populares, para luego poder crear una única tabla.

    Args:
        -cine: Clase Archivo que hace referencia a los datos obtenidos del cine.
        -museo: Clase Archivo que hace referencia a los datos obtenidos del museo.
        -biblioteca: Clase Archivo que hace referencia a los datos obtenidos de la biblioteca.

    Retorna el dataframe generado por la concatenacion de los datos de las tres categorías.
    """
    
    try:
        df = pd.concat(
            [
                cine.dataframe_archivo_informacion(CAMPOS_CINE, CAMPOS_REEMPLAZO_CINE),
                museo.dataframe_archivo_informacion(CAMPOS_MUSEO, CAMPOS_REEMPLAZO_MUSEO),
                biblioteca.dataframe_archivo_informacion(CAMPOS_BIBLIOTECA, CAMPOS_REEMPLAZO_BIBLIOTECA)],
            ignore_index=True
        )
        logging.info("Se procesó la información de los archivos correctamente")
        return df
    except Exception as err:
        logging.error("Error inesperado en la descarga de archivos: " + str(err))


def cantidad_registros(cine: Archivo, museo: Archivo, biblioteca: Archivo):
    try:

        """
        Cantidad de registros

        Función que procesa los datos conjuntos para luego poder generar una tabla.
        
        Args:
        -cine: Clase Archivo que hace referencia a los datos obtenidos del cine.
        -museo: Clase Archivo que hace referencia a los datos obtenidos del museo.
        -biblioteca: Clase Archivo que hace referencia a los datos obtenidos de la biblioteca.

        Retorna el dataframe generado por la concatenacion de los datos de las tres categorías.
        """

        df = pd.concat([
            cine.dataframe_registros(["categoria"]),
            cine.dataframe_registros(["Fuente"]),
            cine.dataframe_registros(["categoria", "Fuente"]),
            biblioteca.dataframe_registros(["categoria"]),
            biblioteca.dataframe_registros(["Fuente"]),
            biblioteca.dataframe_registros(["categoria", "Fuente"]),
            museo.dataframe_registros(["categoria"]),
            museo.dataframe_registros(["fuente"]),
            museo.dataframe_registros(["categoria", "fuente"])
        ])
        logging.info("Se procesó la cantidad de registros correctamente")
        return df
    except Exception as err:
        logging.error("Error inesperado en la descarga de archivos: " + str(err))

def agrupador_cine(cine: Archivo):

    """
        Agrupador para archivo de cine.

        Función que procesa la información de cines para luegi poder crear una tabla que contenga cierta información.
        
        Args:
        -cine: Clase Archivo que hace referencia a los datos obtenidos del cine.

        Retorna el dataframe procesado.
    """

    try:
        df = cine.dataframe_agrupador_cine()
        logging.info("Se procesó la información de los cines correctamente")
        return df
    except Exception as err:
        logging.error("Error inesperado en la descarga de archivos: " + str(err))


def main():
    bd = BaseDeDatos()
    cine, museo, biblioteca = descarga_archivos()
    bd.write(procesar_informacion(cine, museo, biblioteca), "datosprocesados")
    bd.write(cantidad_registros(cine, museo, biblioteca), "cantidadregistros")
    bd.write(agrupador_cine(cine), "informacioncines")


if __name__ == '__main__':
    main()