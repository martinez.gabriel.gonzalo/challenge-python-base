MESES = {
    1:"Enero",
    2:"Febrero",
    3:"Marzo",
    4:"Abril",
    5:"Mayo",
    6:"Junio",
    7:"Julio",
    8:"Agosto",
    9:"Septiembre",
    10:"Octubre",
    11:"Noviebre",
    12:"Diciembre"
}

CAMPOS_CINE = [
    "Cod_Loc",
    "IdProvincia",
    "IdDepartamento",
    "categoria",
    "Provincia",
    "Localidad",
    "Nombre",
    "Dirección",
    "CP",
    "Teléfono",
    "Mail",
    "Web",
    "fecha_carga"]

CAMPOS_REEMPLAZO_CINE = {
    "Cod_Loc":"cod_localidad",
    "IdProvincia":"id_provincia",
    "IdDepartamento":"id_departamento",
    "Provincia":"provincia",
    "Localidad":"localidad",
    "Nombre":"nombre",
    "Dirección":"domicilio",
    "CP":"codigo_postal",
    "Teléfono":"numero_de_telefono",
    "Mail":"mail",
    "Web":"web"
}

CAMPOS_MUSEO = [
    "Cod_Loc",
    "IdProvincia",
    "IdDepartamento",
    "categoria",
    "provincia",
    "localidad",
    "nombre",
    "direccion",
    "CP",
    "telefono",
    "Mail",
    "Web",
    "fecha_carga"]

CAMPOS_REEMPLAZO_MUSEO = {
    "Cod_Loc":"cod_localidad",
    "IdProvincia":"id_provincia",
    "IdDepartamento":"id_departamento",
    "direccion":"domicilio",
    "CP":"codigo_postal",
    "Teléfono":"numero_de_telefono",
    "Mail":"mail",
    "Web":"web"
}

CAMPOS_BIBLIOTECA = [
    "Cod_Loc",
    "IdProvincia",
    "IdDepartamento",
    "categoria",
    "Provincia",
    "Localidad",
    "Nombre",
    "Domicilio",
    "CP",
    "Teléfono",
    "Mail",
    "Web",
    "fecha_carga"]

CAMPOS_REEMPLAZO_BIBLIOTECA = {
    "Cod_Loc":"cod_localidad",
    "IdProvincia":"id_provincia",
    "IdDepartamento":"id_departamento",
    "Provincia":"provincia",
    "Localidad":"localidad",
    "Nombre":"nombre",
    "Domicilio":"domicilio",
    "CP":"codigo_postal",
    "Teléfono":"numero_de_telefono",
    "Mail":"mail",
    "Web":"web"
}